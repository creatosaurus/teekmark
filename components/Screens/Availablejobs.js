import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Image } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Checkcolor from '../../assets/icons/Checkcolor.svg'
import Availablejobscard from '../Reusable Components/Availablejobscard'
import Aboutcompanicard from '../Reusable Components/Aboutcomanicard'
import Companiescard from '../Reusable Components/Companiescard'
import Axios from 'axios'
import constant from '../../constant'
import AsyncStorage from '@react-native-async-storage/async-storage'

const Availablejobs = (props) => {

    const [activeValue, setActiveValue] = useState(0)
    const [availableJobs, setavailableJobs] = useState([])

    const getAvailableJobs = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                filter: {
                    category_id: props.route.params.data.category_id
                },
                queryString: ""
            }
            const response = await Axios.post(constant.url + "search/searchjobs/", body, config)
            setavailableJobs(response.data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getAvailableJobs()
    }, [])


    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Navigationback title="Available jobs" back={() => props.navigation.goBack()} />
                <View style={styles.subContainer}>
                    <View style={{ position: 'relative' }}>
                        <Image style={{ width: 35, height: 35 }}
                            source={{ uri: props.route.params.data.company_logo }} />
                        <Checkcolor width={10} height={10} style={styles.checkIcon} />
                    </View>
                    <Text style={styles.subContainerTitle}>{props.route.params.data.company_name}</Text>
                    <Text style={styles.subContainerSubtitle}>{props.route.params.data.address}</Text>
                </View>

                <View style={styles.tabBar}>
                    <TouchableOpacity onPress={() => setActiveValue(0)}>
                        <Text style={activeValue === 0 ? styles.activeText : styles.tab}>Openings</Text>
                        <View style={activeValue === 0 ? styles.active : null} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setActiveValue(1)}>
                        <Text style={activeValue === 1 ? styles.activeText : styles.tab}>About</Text>
                        <View style={activeValue === 1 ? styles.active : null} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setActiveValue(2)}>
                        <Text style={activeValue === 2 ? styles.activeText : styles.tab}>Similar</Text>
                        <View style={activeValue === 2 ? styles.active : null} />
                    </TouchableOpacity>
                </View>

                <View>
                    {
                        activeValue === 0 ? <View>
                            {
                                props.route.params.data.availableJobs.map(data => {
                                    return <Availablejobscard
                                        data={data}
                                        navigation={props.navigation}
                                        key={data._id} />
                                })
                            }
                        </View> : activeValue === 1 ? <View>
                            <Aboutcompanicard
                                status={props.route.params.data.status}
                                category={props.route.params.data.status}
                                companyType={props.route.params.data.company_type}
                                overview={props.route.params.data.about}
                                contactNo={props.route.params.data.contact_no}
                                email={props.route.params.data.email}
                                website={props.route.params.data.website}
                                location={props.route.params.data.address}
                                facebook={props.route.params.data.facebook}
                                linkedIn={props.route.params.data.linkedIn}
                            />
                        </View> : <View>
                                    {
                                        availableJobs.map(data => {
                                            return <Companiescard
                                                onPress={() => props.navigation.navigate("jobdescription", {
                                                    data: data
                                                })}
                                                companyName={data.company_id.company_name}
                                                location={data.city_id.city}
                                                opening={data.openings}
                                                navigation={props.navigation}
                                                url={data.company_id.company_logo}
                                                data={data}
                                                duration={data.duration}
                                                updatedAt = {data.updatedAt}
                                                jobtype={data.jobtype_id === undefined ? "NA" : data.jobtype_id.jobtype}
                                                key={data._id}
                                            />
                                        })
                                    }
                                </View>
                    }
                </View>
            </ScrollView>
        </View>
    )
}

export default Availablejobs

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 10
    },
    subContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '10%'
    },
    subContainerTitle: {
        fontFamily: 'Roboto-Medium',
        fontSize: 18,
        marginTop: 5
    },
    subContainerSubtitle: {
        color: '#8B8B8B',
        fontSize: 12,
        marginTop: 5
    },
    checkIcon: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    tabBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: '10%',
        marginTop: 20
    },
    tab: {
        color: '#919191'
    },
    active: {
        borderColor: '#F15C20',
        borderWidth: 3,
        borderRadius: 10,
        marginTop: 3
    },
    activeText: {
        color: '#000',
        fontFamily: 'Roboto-Medium'
    }
})
