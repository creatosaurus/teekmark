import React, { useState, useContext } from 'react'
import Axios from 'axios'
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions } from 'react-native'
import InputBox from '../Reusable Components/InputBox'
import Logo from '../../assets/icons/Logo.png'
import constant from '../../constant'
import { ScrollView } from 'react-native-gesture-handler'
import Loading from './Loading'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { UserContext } from '../../AuthContext'
const { width, height } = Dimensions.get('window');

const Login = (props) => {

    const [email, setEmail] = useState(null)
    const [password, setPassword] = useState(null)
    const [loading, setloading] = useState(false)
    const [showPassword, setshowPassword] = useState(true)
    const { logIn } = useContext(UserContext)


    const logUserIn = async () => {
        // check the user have fill all the input fields
        if (email === null || password === null) return alert("Plz fill all the fields")
        setloading(true)
        try {
            const response = await Axios.post(constant.url + "user/login", {
                email: email.trim(),
                password: password.trim()
            })
            if (response.status === 200) {
                await AsyncStorage.setItem("token", response.data.token)
                logIn()
            }
            setloading(false)
        } catch (error) {
            setloading(false)
            setEmail(null)
            setPassword(null)
            alert(error.response.data.message)
        }
    }


    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> : <View style={styles.container}>
                    <View style={styles.header}>
                        <Image source={Logo} style={styles.logo} />
                        <Text style={styles.headerTitle}>Teekmark</Text>
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.inputContainerTitle}>Welcome Back,</Text>
                            <Text style={styles.inputContainerSubTitle}>Sign in to Continue</Text>
                            <InputBox placeholder="Enter your Email address"
                                title="Email"
                                keyboardType="email-address"
                                autoCapitalize="none"
                                onChangeText={text => setEmail(text)} />
                            <View>
                                <InputBox placeholder="Enter Password" password={showPassword} title="Password"
                                    onChangeText={text => setPassword(text)} />
                                <TouchableOpacity style={styles.viewContainer} onPress={() => setshowPassword(!showPassword)}>
                                    <Image source={require('../../assets/icons/view.png')}
                                        style={styles.view} />
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={() => props.navigation.navigate("forgotpassword")}>
                                <Text style={styles.forgot}>Forget Password?</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.bottomContainer}>
                            <TouchableOpacity style={styles.button} onPress={logUserIn}>
                                <Text style={{ color: '#fff' }}>Login</Text>
                            </TouchableOpacity>

                            <View style={styles.linkContainer}>
                                <Text style={styles.info}>No account yet?</Text>
                                <TouchableOpacity onPress={() => props.navigation.navigate("signup")}>
                                    <Text style={styles.login}>Sign Up</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                    <Text style={styles.footerTitle}>{`Copyright @ 2020. TeekmarkAll\nrights reserved`}</Text>
                </View>
            }
        </React.Fragment>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        position: 'relative',
        height: height - 20
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 10
    },
    headerTitle: {
        fontSize: 20,
        fontFamily: 'Poppins-Medium'
    },
    inputContainer: {
        marginTop: '4%'
    },
    inputContainerTitle: {
        fontSize: 26,
        fontFamily: 'Roboto-Bold'
    },
    inputContainerSubTitle: {
        fontSize: 18,
        fontFamily: 'Roboto-Light',
        marginTop: 10
    },
    bottomContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    forgot: {
        alignSelf: 'flex-end',
        fontFamily: 'Roboto-Regular'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: '40%',
        backgroundColor: '#F15C20',
        borderRadius: 10
    },
    linkContainer: {
        flexDirection: 'row',
        marginTop: 20
    },
    info: {
        fontSize: 16,
        fontFamily: "Roboto-Medium",
        color: '#000'
    },
    login: {
        color: '#F15C20',
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
    },
    footerTitle: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center',
        position: 'absolute',
        bottom: 20,
        alignSelf: 'center'
    },
    logo: {
        width: 60,
        height: 45
    },
    view: {
        height: 16,
        width: 16,
    },
    viewContainer: {
        position: 'absolute',
        right: 20,
        bottom: 0,
        height: 53,
        width: 53,
        justifyContent: 'center',
        alignItems: 'center'
    }
})