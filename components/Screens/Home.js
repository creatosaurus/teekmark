import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, Text, TextInput, View, ScrollView, TouchableOpacity, ActivityIndicator, Image } from 'react-native'
import Jobcategorycard from '../Reusable Components/Jobcategorycard'
import Jobsforyoucard from '../Reusable Components/Jobsforyoucard'
import Popularjobcard from '../Reusable Components/Popularjobcard'
import Drawerlayout from '../Reusable Components/Drawerlayout'
import Tabbar from '../Reusable Components/Tabbar'
import Finger from '../../assets/icons/two-fingers.svg'
import Menu from '../../assets/icons/black_menu.png'
import Arrow from '../../assets/icons/arrow.png'
import Settings from '../../assets/icons/Settings.svg'
import { Picker } from '@react-native-picker/picker'
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Loading from './Loading'
import Global from '../../Global'
import { UserContext } from '../../AuthContext'


const Home = (props) => {

    const [openFilter, setopenFilter] = useState(false)
    const [jobs, setjobs] = useState([])
    const [loadingjob, setloadingjob] = useState(false)
    const [category, setcategory] = useState([])
    const [loadingcategory, setloadingcategory] = useState(false)
    const [loadingjobforyou, setloadingjobforyou] = useState(false)
    const [jobforyou, setjobforyou] = useState([])
    const [jobCategory, setjobCategory] = useState([])
    const [subCategory, setsubCategory] = useState([])
    const [location, setlocation] = useState([])
    const [designation, setdesignation] = useState([])
    const [selectedCategory, setselectedCategory] = useState(null)
    const [selectedSubCategory, setselectedSubCategory] = useState(null)
    const [selectedLocation, setselectedLocation] = useState(null)
    const [selectedExperience, setselectedExperience] = useState(null)
    const [selectedDesignation, setselectedDesignation] = useState(null)
    const [query, setquery] = useState("")
    const [loading, setloading] = useState(false)
    const [refresh, setrefresh] = useState(false)
    const { signOut } = useContext(UserContext)
    const [showDrawer, setshowDrawer] = useState(false)

    const getTheUser = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                id: decoded.id,
            }
            const response = await Axios.post(constant.url + "User/getUserProfile", body, config)
            Global.user = response.data
            setrefresh(!refresh)
        } catch (error) {
            console.log(error)
        }
    }

    const getPopularJobs = async () => {
        try {
            setloadingjob(true)
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "jobs/getpopularjobs", {
                headers: {
                    "x-access-token": token
                }
            })
            setjobs(response.data.jobs)
            setloadingjob(false)
        } catch (error) {
            alert(error)
            console.log(error.response.data)
            setloadingjob(false)
        }
    }

    const getCategory = async () => {
        try {
            setloadingcategory(true)
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "category/getActiveCategory", {
                headers: {
                    "x-access-token": token
                }
            })
            setcategory(response.data.category)
            setloadingcategory(false)
        } catch (error) {
            alert(error)
            setloadingcategory(false)
        }
    }

    const getJobCategory = async () => {
        try {
            const response = await Axios.get(constant.url + "category/getActiveCategory")
            setjobCategory(response.data.category)
        } catch (error) {
            console.log(error)
        }
    }

    const getActiveCity = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "city/getactivecity", {
                headers: {
                    "x-access-token": token
                }
            })
            setlocation(response.data.city)
        } catch (error) {

        }
    }

    const getSubCategory = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(category.url + "getActivesubCategory/5fdc5d16d4bcfb3a388307c6", {
                headers: {
                    "x-access-token": token
                }
            })
            setsubCategory(response.data.category)
        } catch (error) {

        }
    }

    const getDesignation = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "designation/getdesignation", {
                headers: {
                    "x-access-token": token
                }
            })
            setdesignation(response.data.designation)
        } catch (error) {

        }
    }

    const getJobForYou = async () => {
        try {
            setloadingjobforyou(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const response = await Axios.get(constant.url + "jobs/jobsforyou/" + decoded.id, {
                headers: {
                    "x-access-token": token
                }
            })
            setjobforyou(response.data.jobs)
            setloadingjobforyou(false)
        } catch (error) {
            setloadingjobforyou(false)
        }
    }

    const tokenIsExpired = async () => {
        const token = await AsyncStorage.getItem('token');
        const decoded = jwt_decode(token);
        if (Date.now() >= decoded.exp * 1000) {
            signOut()
        } else {
            getTheUser()
            getPopularJobs()
            getCategory()
            getJobCategory()
            getActiveCity()
            getSubCategory()
            getDesignation()
            getJobForYou()
        }
    }

    useEffect(() => {
        tokenIsExpired()
    }, [])


    const applyFilter = async () => {
        try {
            setloading(true)
            setopenFilter(false)
            const filters = {
                category_id: selectedCategory,
                jobtype_id: null,
                city_id: selectedLocation,
                sub_category_id: selectedSubCategory,
                jobtype_id: selectedDesignation
            }
            const removeEmptyValues = obj => (
                JSON.parse(JSON.stringify(obj, (k, v) => v ?? undefined))
            )
            const data = removeEmptyValues(filters)
            const token = await AsyncStorage.getItem('token');

            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }

            const body = {
                filters: data,
                queryString: query,
            }
            const response = await Axios.post(constant.url + "search/searchjobs/", body, config)
            props.navigation.navigate('search', {
                data: response.data,
                category: category,
                subCategory: subCategory,
                designation: designation,
                location: location,
            })
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }


    const filterCategory = async (id) => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }

            const body = {
                filters: {
                    category_id: id
                },
                queryString: "",
            }
            const response = await Axios.post(constant.url + "search/searchjobs/", body, config)
            props.navigation.navigate('search', {
                data: response.data,
                category: category,
                subCategory: subCategory,
                designation: designation,
                location: location,
            })
            setloading(false)
        } catch (error) {
            setloading(false)
            alert(error)
        }
    }

    const searchTag = async (tag) => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }

            const body = {
                queryString: tag,
            }
            const response = await Axios.post(constant.url + "search/searchjobs/", body, config)
            props.navigation.navigate('search', {
                data: response.data,
                category: category,
                subCategory: subCategory,
                designation: designation,
                location: location,
            })
            setloading(false)
        } catch (error) {
            alert(error)
            setloading(false)
        }
    }

    const logOutUser = () => {
        signOut()
    }

    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> :
                    <View style={openFilter === true ? { ...styles.container, backgroundColor: 'rgba(0, 0, 0, 0.1)' } : { ...styles.container }}>
                        {
                            showDrawer === false ? null : <Drawerlayout navigate={props.navigation.navigate}
                                logOutUser={logOutUser}
                                close={() => setshowDrawer(false)} />
                        }
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={styles.headerContainer}>
                                <View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.userName}>Hi {Global.user === null ? null : Global.user.first_name}</Text>
                                        <Finger width={20} height={20} color="black" />
                                    </View>
                                    <Text style={styles.quote}>Find your perfect Job!</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <TouchableOpacity style={styles.notify} onPress={() => props.navigation.navigate('notifications')}>
                                        <Image style={{ width: 20, height: 20 }} source={require('../../assets/icons/bell.png')}  />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.notify} onPress={() => setshowDrawer(true)}>
                                        <Image style={{ width: 20, height: 20 }} source={Menu} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={styles.searchContainer}>
                                <View style={styles.search} >
                                    <TextInput placeholder="What are you looking for ?"
                                        placeholderTextColor="#919191"
                                        onChangeText={(text) => setquery(text)}
                                    />
                                    <TouchableOpacity onPress={() => applyFilter()}>
                                        <Image source={Arrow} style={styles.arrow} />
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity style={styles.filter} onPress={() => setopenFilter(true)}>
                                    <Settings width={20} height={20} color="black" />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.tagContainer}>
                                <TouchableOpacity style={styles.tag} onPress={() => searchTag("product")}>
                                    <Text style={styles.tagName}>Product</Text>
                                </TouchableOpacity >
                                <TouchableOpacity
                                    style={{ ...styles.tag, marginLeft: 10 }}
                                    onPress={() => searchTag("design")}>
                                    <Text style={styles.tagName}>Design</Text>
                                </TouchableOpacity >
                                <TouchableOpacity
                                    style={{ ...styles.tag, marginLeft: 10 }}
                                    onPress={() => searchTag("development")}>
                                    <Text style={styles.tagName}>Development</Text>
                                </TouchableOpacity >
                            </View>

                            <View style={styles.popularJobs}>
                                <View style={styles.popularJobsTitleContainer}>
                                    <Text style={styles.jobTitle}>Popular Job</Text>
                                </View>
                                {
                                    loadingjob === true ? <ActivityIndicator size="large" color="#dcdcdc" /> :
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                            {
                                                jobs.map(data => {
                                                    return <Popularjobcard
                                                        data={data}
                                                        key={data._id}
                                                        companyName={data.company_id.company_name}
                                                        jobRole={data.job_profile}
                                                        location={data.company_id.address}
                                                        url={data.company_id.company_logo}
                                                        navigation={props.navigation}
                                                        salary={data.ctc} />
                                                })
                                            }
                                        </ScrollView>
                                }
                            </View>

                            <View style={styles.popularJobs}>
                                <View style={styles.popularJobsTitleContainer}>
                                    <Text style={styles.jobTitle}>Job category</Text>
                                </View>
                                {
                                    loadingcategory === true ? <ActivityIndicator size="large" color="#dcdcdc" /> :
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                            {
                                                category.map(data => {
                                                    return <Jobcategorycard
                                                        key={data._id}
                                                        id={data._id}
                                                        filterCategory={filterCategory}
                                                        category={data.category}
                                                        url={data.imageurl} />
                                                })
                                            }
                                        </ScrollView>
                                }
                            </View>

                            <View style={{ ...styles.popularJobs, marginBottom: 75 }}>
                                <View style={styles.popularJobsTitleContainer}>
                                    <Text style={styles.jobTitle}>Jobs for you</Text>
                                    <TouchableOpacity onPress={applyFilter}>
                                        <Text style={styles.jobSubTitle}>See All</Text>
                                    </TouchableOpacity>
                                </View>
                                {
                                    loadingjobforyou === true ? <ActivityIndicator size="large" color="#dcdcdc" /> :
                                        <ScrollView horizontal={false} showsVerticalScrollIndicator={false}>
                                            {
                                                jobforyou.map(data => {
                                                    return <Jobsforyoucard
                                                        data={data}
                                                        navigation={props.navigation}
                                                        key={data._id} />
                                                })
                                            }
                                        </ScrollView>
                                }
                            </View>
                        </ScrollView>
                        <Tabbar active="home" navigate={props.navigation.navigate} />
                        {
                            openFilter === false ? null : <View style={styles.filterContainer}>
                                <ScrollView>
                                    <TouchableOpacity style={styles.closeButton} onPress={() => setopenFilter(!openFilter)}>
                                        <Text style={{ color: '#fff' }}>x</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.heading}>Set Filters</Text>
                                    <View>
                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Category</Text>
                                            <Picker
                                                selectedValue={selectedCategory}
                                                onValueChange={(itemValue) => {
                                                    setselectedCategory(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="category" value={null} />
                                                {
                                                    jobCategory.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.category} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Sub Category</Text>
                                            <Picker
                                                selectedValue={selectedSubCategory}
                                                onValueChange={(itemValue) => {
                                                    setselectedSubCategory(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="Select Subcategory" value={null} />
                                                {
                                                    subCategory.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.subcategory} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Location</Text>
                                            <Picker
                                                selectedValue={selectedLocation}
                                                onValueChange={(itemValue) => {
                                                    setselectedLocation(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="Select Location" value={null} />
                                                {
                                                    location.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.city} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Experience</Text>
                                            <Picker
                                                selectedValue={selectedExperience}
                                                onValueChange={(itemValue) => {
                                                    setselectedExperience(itemValue)
                                                }}>
                                                <Picker.Item label="0 Year" value="0 Year" />
                                                <Picker.Item label="1 Year" value="1 Year" />
                                                <Picker.Item label="2 Year" value="2 Year" />
                                                <Picker.Item label="3 Year" value="3 Year" />
                                                <Picker.Item label="4 Year" value="4 Year" />
                                                <Picker.Item label="5 Year" value="5 Year" />
                                                <Picker.Item label="6 Year" value="6 Year" />
                                                <Picker.Item label="7 Year" value="7 Year" />
                                                <Picker.Item label="8 Year" value="8 Year" />
                                                <Picker.Item label="9 Year" value="9 Year" />
                                                <Picker.Item label="10 Year" value="10 Year" />
                                                <Picker.Item label="11 Year" value="11 Year" />
                                                <Picker.Item label="12 Year" value="12 Year" />
                                                <Picker.Item label="13 Year" value="13 Year" />
                                                <Picker.Item label="14 Year" value="14 Year" />
                                                <Picker.Item label="15 Year" value="15 Year" />
                                                <Picker.Item label="16 Year" value="16 Year" />
                                                <Picker.Item label="17 Year" value="17 Year" />
                                                <Picker.Item label="18 Year" value="18 Year" />
                                                <Picker.Item label="19 Year" value="19 Year" />
                                                <Picker.Item label="20 Year" value="20 Year" />
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Job Type</Text>
                                            <Picker
                                                selectedValue={selectedDesignation}
                                                onValueChange={(itemValue) => {
                                                    setselectedDesignation(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="Select job type" value={null} />
                                                {
                                                    designation.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.designation} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>
                                    </View>

                                    <TouchableOpacity style={styles.button} onPress={applyFilter}>
                                        <Text style={{ color: '#fff' }}>Apply Filters</Text>
                                    </TouchableOpacity>
                                </ScrollView>
                            </View>
                        }
                    </View>
            }
        </React.Fragment>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    headerContainer: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    userName: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        marginRight: 10
    },
    quote: {
        fontFamily: 'Roboto-Medium',
        fontSize: 20
    },
    searchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10
    },
    search: {
        backgroundColor: '#F4F6F7',
        borderRadius: 10,
        paddingHorizontal: 10,
        width: '84%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    filter: {
        backgroundColor: "#F15C20",
        padding: 13,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    tagContainer: {
        marginTop: 15,
        flexDirection: 'row',
    },
    tag: {
        borderColor: '#B2B2B2',
        borderWidth: 1,
        borderRadius: 10,
        paddingHorizontal: 15,
        paddingVertical: 3,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Roboto-Regular',
    },
    tagName: {
        color: '#919191'
    },
    popularJobs: {
        marginTop: 20,
    },
    popularJobsTitleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10
    },
    jobTitle: {
        fontSize: 20,
        fontFamily: 'Roboto-Medium',
    },
    jobSubTitle: {
        color: '#919191',
        fontFamily: 'Roboto-Regular',
    },
    filterContainer: {
        backgroundColor: '#fff',
        position: 'absolute',
        zIndex: 1,
        bottom: 0,
        left: 0,
        right: 0,
        padding: 10,
        height: '65%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    heading: {
        alignSelf: 'center',
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
        marginTop: 10
    },
    pickerContainer: {
        borderColor: '#DADADA',
        borderWidth: 1,
        borderRadius: 5,
        position: 'relative',
        marginTop: 20,
    },
    headePicker: {
        position: 'absolute',
        color: '#919191',
        backgroundColor: '#fff',
        top: -20,
        padding: 10,
        left: 20,
        fontFamily: 'Roboto-Regular',
    },
    picker: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
    },
    button: {
        backgroundColor: '#F15C20',
        borderRadius: 10,
        paddingHorizontal: 30,
        paddingVertical: 15,
        alignSelf: 'center',
        marginTop: 20
    },
    closeButton: {
        backgroundColor: '#F15C20',
        height: 35,
        width: 35,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 20
    },
    notify: {
        backgroundColor: '#F3F3F3',
        borderRadius: 10,
        padding: 13,
        marginLeft:10
    },
    arrow: {
        width: 30,
        height: 30,
    }
})
