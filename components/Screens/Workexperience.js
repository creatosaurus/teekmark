import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, Switch, ScrollView, TouchableOpacity, ActivityIndicator, Image, Platform } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Toast from 'react-native-simple-toast';
import Calender from '../../assets/icons/calendar.png'
import DateTimePicker from '@react-native-community/datetimepicker';

const Workexperience = (props) => {

    const [position, setposition] = useState(null)
    const [jobType, setjobType] = useState(null)
    const [companyName, setcompanyName] = useState(null)
    const [startDate, setstartDate] = useState(null)
    const [endDate, setendDate] = useState(null)
    const [ctc, setctc] = useState(null)
    const [workingStatus, setworkingStatus] = useState(false)
    const [loading, setloading] = useState(false)
    const [show, setshow] = useState(false)
    const [show1, setshow1] = useState(false)

    const toggleSwitch = () => setworkingStatus(previousState => !previousState);

    useEffect(() => {
        let date1 = formatDate(new Date())
        let date2 = formatDate(new Date())
        setendDate(date2)
        setstartDate(date1)
        if (props.route.params.edit === true) {
            console.log(props.route.params.user)
            setcompanyName(props.route.params.user.company_name)
            setposition(props.route.params.user.job_title)
            setjobType(props.route.params.user.job_type)
            setstartDate(props.route.params.user.start_date)
            if (props.route.params.user.working_status === "true") {
                setworkingStatus(true)
            } else {
                setworkingStatus(false)
            }
            if (props.route.params.user.end_date === undefined) {
                setendDate("")
            } else {
                setendDate(props.route.params.user.end_date)
            }
            setctc(props.route.params.user.ctc.toString())
        }
    }, [])

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setshow(Platform.OS === 'ios');
        let date = formatDate(currentDate)
        setstartDate(date);
    };

    const onChange1 = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setshow1(Platform.OS === 'ios');
        let date = formatDate(currentDate)
        setendDate(date);
    };

    const formatDate = (date) => {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }

    const update = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }

            const body = {
                user_id: decoded.id,
                company_name: companyName,
                job_title: position,
                job_type: jobType,
                start_date: startDate,
                end_date: endDate,
                working_status: workingStatus,
                ctc: ctc,
            }

            await Axios.post(constant.url + "User/addUserWorkExp", body, config)
            Toast.show("pull the page to refresh");
            props.navigation.goBack()
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    const updateEdited = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }

            const body = {
                _id: props.route.params.user._id,
                company_name: companyName,
                job_title: position,
                job_type: jobType,
                start_date: startDate,
                end_date: endDate,
                working_status: workingStatus,
                ctc: ctc,
            }

            await Axios.post(constant.url + "User/updateUserWorkExp", body, config)
            Toast.show("pull the page to refresh");
            props.navigation.goBack()
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }

    return (
        <React.Fragment>
            {
                loading === true ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#828282" />
                </View> :
                    <View style={styles.container}>
                        <Navigationback title="Add work experience" back={() => props.navigation.goBack()} />
                        <ScrollView>
                            <View style={{ marginTop: 20, marginBottom: 20 }}>
                                <View style={styles.inputContainer}>
                                    <TextInput
                                        value={companyName}
                                        placeholder="Enter company name"
                                        placeholderTextColor="#B2B2B2"
                                        onChangeText={(text) => setcompanyName(text)} />
                                    <Text style={styles.lable}>Company Name</Text>
                                </View>

                                <View style={styles.inputContainer}>
                                    <TextInput value={position}
                                        placeholder="Enter job title"
                                        placeholderTextColor="#B2B2B2"
                                        onChangeText={(text) => setposition(text)} />
                                    <Text style={styles.lable}>Postion</Text>
                                </View>

                                <View style={styles.inputContainer}>
                                    <TextInput
                                        value={jobType}
                                        placeholder="Enter job type (full time - part time)"
                                        placeholderTextColor="#B2B2B2"
                                        onChangeText={(text) => setjobType(text)} />
                                    <Text style={styles.lable}>Job Type</Text>
                                </View>

                                <TouchableOpacity
                                    onPress={() => setshow(true)}
                                    style={[styles.inputContainer, { justifyContent: 'center' }]}>
                                    <Text>{startDate}</Text>
                                    <Image source={Calender} style={styles.calender} />
                                    <Text style={styles.lable}>Start Date</Text>
                                </TouchableOpacity>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, alignItems: 'center' }}>
                                    <Text>Currently working here</Text>
                                    <Switch
                                        trackColor={{ false: "#dcdcdc", true: "#81b0ff" }}
                                        thumbColor={workingStatus ? "#f5dd4b" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={toggleSwitch}
                                        value={workingStatus}
                                    />
                                </View>

                                <TouchableOpacity
                                    onPress={() => setshow1(true)}
                                    style={[styles.inputContainer, { justifyContent: 'center' }]}>
                                    <Text>{endDate}</Text>
                                    <Image source={Calender} style={styles.calender} />
                                    <Text style={styles.lable}>End Date</Text>
                                </TouchableOpacity>

                                <View style={styles.inputContainer}>
                                    <TextInput
                                        onChangeText={(text) => setctc(text)}
                                        keyboardType="numeric"
                                        value={ctc}
                                        placeholder="Enter you previous CTC"
                                        placeholderTextColor="#B2B2B2" />
                                    <Text style={styles.lable}>CTC</Text>
                                </View>
                            </View>
                        </ScrollView>
                        {
                            props.route.params.edit === true ?
                                <TouchableOpacity style={styles.button} onPress={updateEdited}>
                                    <Text style={{ color: '#fff', fontSize: 16 }}>Update</Text>
                                </TouchableOpacity> :
                                <TouchableOpacity style={styles.button} onPress={update}>
                                    <Text style={{ color: '#fff', fontSize: 16 }}>Update</Text>
                                </TouchableOpacity>
                        }
                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={new Date()}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                            />
                        )}
                        {show1 && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={new Date()}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={onChange1}
                            />
                        )}
                    </View>

            }
        </React.Fragment>
    )
}

export default Workexperience

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    inputContainer: {
        borderColor: '#DADADA',
        borderRadius: 5,
        borderWidth: 1,
        position: 'relative',
        paddingHorizontal: 10,
        marginTop: 25,
        height: 52
    },
    lable: {
        position: 'absolute',
        top: -14,
        color: '#919191',
        backgroundColor: '#fff',
        left: 30,
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#F15C20',
        borderRadius: 10,
        paddingHorizontal: 40,
        paddingVertical: 15,
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 20,
        marginTop: 10
    },
    calender: {
        width: 26,
        height: 26,
        position: 'absolute',
        bottom: 12,
        right: 10
    }
})
