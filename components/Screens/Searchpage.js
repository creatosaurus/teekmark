import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, TextInput, ScrollView, ActivityIndicator } from 'react-native'
import Settings from '../../assets/icons/Settings.svg'
import Searchresultcard from '../Reusable Components/Searchresultcard'
import Navigationback from '../Reusable Components/Navigationback'
import Axios from 'axios'
import { Picker } from '@react-native-picker/picker'
import constant from '../../constant'

const Searchpage = (props) => {

    const [data, setdata] = useState([])
    const [openFilter, setopenFilter] = useState(false)
    const [selectedCategory, setselectedCategory] = useState(null)
    const [selectedSubCategory, setselectedSubCategory] = useState(null)
    const [selectedLocation, setselectedLocation] = useState(null)
    const [selectedExperience, setselectedExperience] = useState(null)
    const [selectedDesignation, setselectedDesignation] = useState(null)
    const [loading, setloading] = useState(false)
    const [query, setquery] = useState(null)

    useEffect(() => {
        setdata(props.route.params.data)
    }, [])

    const applyFilter = async () => {
        setopenFilter(false)
        setloading(true)
        try {
            const filters = {
                category_id: selectedCategory,
                jobtype_id: null,
                city_id: selectedLocation,
                sub_category_id: selectedSubCategory,
                jobtype_id: selectedDesignation
            }
            const removeEmptyValues = obj => (
                JSON.parse(JSON.stringify(obj, (k, v) => v ?? undefined))
            )
            const data = removeEmptyValues(filters)
            const response = await Axios.post(constant.url + "search/searchjobs/", {
                filters: data,
                queryString: query
            })
            setdata(response.data)
            setloading(false)
        } catch (error) {
            console.log(error)
            setloading(false)
        }
    }

    const likeTheJob = async (id) => {
        try {
            const addedLikeData = data.map(data => {
                if (data._id === id) {
                    data.job_shortlisted = true
                    return data
                } else {
                    return data
                }
            })
            setdata(addedLikeData)
        } catch (error) {

        }
    }

    return (
        <React.Fragment>
            {
                loading === true ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#828282" />
                </View> :
                    <View style={openFilter === true ? { ...styles.container, backgroundColor: 'rgba(0, 0, 0, 0.1)' } : { ...styles.container }}>
                        <Navigationback title="Search Result" back={() => props.navigation.goBack()} />

                        <View style={styles.searchContainer}>
                            <View>
                                <TextInput placeholder="What are you looking for ?"
                                    onChangeText={(text) => setquery(text)}
                                    placeholderTextColor="#919191"
                                    style={styles.search} />
                            </View>
                            <TouchableOpacity style={styles.filter} onPress={() => setopenFilter(true)}>
                                <Settings width={20} height={20} color="black" />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.foundTitle}>{data.length} job oppurtunities found</Text>

                        <ScrollView showsVerticalScrollIndicator={false}>
                            {
                                data.map(item => {
                                    return <Searchresultcard
                                        likeTheJob={likeTheJob}
                                        navigation={props.navigation}
                                        companyName={item.company_id.company_name}
                                        uri={item.company_id.company_logo}
                                        jobProfile={item.job_profile}
                                        city={item.city_id.city}
                                        ctc={item.ctc}
                                        data={item}
                                        liked={false}
                                        key={item._id} />
                                })
                            }
                        </ScrollView>
                        {
                            openFilter === false ? null : <View style={styles.filterContainer}>
                                <ScrollView>
                                    <TouchableOpacity style={styles.closeButton} onPress={() => setopenFilter(!openFilter)}>
                                        <Text style={{ color: '#fff' }}>x</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.heading}>Set Filters</Text>
                                    <View>
                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Category</Text>
                                            <Picker
                                                selectedValue={selectedCategory}
                                                onValueChange={(itemValue) => {
                                                    setselectedCategory(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="category" value={null} />
                                                {
                                                    props.route.params.category.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.category} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Sub Category</Text>
                                            <Picker
                                                selectedValue={selectedSubCategory}
                                                onValueChange={(itemValue) => {
                                                    setselectedSubCategory(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="Select Subcategory" value={null} />
                                                {
                                                    props.route.params.subCategory.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.subcategory} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Location</Text>
                                            <Picker
                                                selectedValue={selectedLocation}
                                                onValueChange={(itemValue) => {
                                                    setselectedLocation(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="Select Location" value={null} />
                                                {
                                                    props.route.params.location.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.city} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Experience</Text>
                                            <Picker
                                                selectedValue={selectedExperience}
                                                onValueChange={(itemValue) => {
                                                    setselectedExperience(itemValue)
                                                }}>
                                                <Picker.Item label="0 Year" value="0 Year" />
                                                <Picker.Item label="1 Year" value="1 Year" />
                                                <Picker.Item label="2 Year" value="2 Year" />
                                                <Picker.Item label="3 Year" value="3 Year" />
                                                <Picker.Item label="4 Year" value="4 Year" />
                                                <Picker.Item label="5 Year" value="5 Year" />
                                                <Picker.Item label="6 Year" value="6 Year" />
                                                <Picker.Item label="7 Year" value="7 Year" />
                                                <Picker.Item label="8 Year" value="8 Year" />
                                                <Picker.Item label="9 Year" value="9 Year" />
                                                <Picker.Item label="10 Year" value="10 Year" />
                                                <Picker.Item label="11 Year" value="11 Year" />
                                                <Picker.Item label="12 Year" value="12 Year" />
                                                <Picker.Item label="13 Year" value="13 Year" />
                                                <Picker.Item label="14 Year" value="14 Year" />
                                                <Picker.Item label="15 Year" value="15 Year" />
                                                <Picker.Item label="16 Year" value="16 Year" />
                                                <Picker.Item label="17 Year" value="17 Year" />
                                                <Picker.Item label="18 Year" value="18 Year" />
                                                <Picker.Item label="19 Year" value="19 Year" />
                                                <Picker.Item label="20 Year" value="20 Year" />
                                            </Picker>
                                        </View>

                                        <View style={styles.pickerContainer}>
                                            <Text style={styles.headePicker}>Job Type</Text>
                                            <Picker
                                                selectedValue={selectedDesignation}
                                                onValueChange={(itemValue) => {
                                                    setselectedDesignation(itemValue)
                                                }}
                                            >
                                                <Picker.Item color="#919191" label="Select job type" value={null} />
                                                {
                                                    props.route.params.designation.map(data => {
                                                        return <Picker.Item key={data._id} color="#000000" label={data.designation} value={data._id} />
                                                    })
                                                }
                                            </Picker>
                                        </View>
                                    </View>

                                    <TouchableOpacity style={styles.button} onPress={applyFilter}>
                                        <Text style={{ color: '#fff' }}>Apply Filters</Text>
                                    </TouchableOpacity>
                                </ScrollView>
                            </View>
                        }
                    </View>
            }
        </React.Fragment>
    )
}

export default Searchpage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        position: 'relative'
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    backButton: {
        width: 40,
        height: 40,
        borderRadius: 10,
        backgroundColor: '#F3F3F3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 20,
        fontFamily: 'Roboto-Medium',
        marginLeft: 20
    },
    searchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: '8%'
    },
    search: {
        backgroundColor: '#F4F6F7',
        borderRadius: 10,
        paddingHorizontal: 10,
        width: '165%'
    },
    filter: {
        backgroundColor: "#F15C20",
        height: 50,
        width: 50,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    foundTitle: {
        fontFamily: 'Roboto-Regular',
        marginTop: 5,
        marginBottom: 10
    },
    filterContainer: {
        backgroundColor: '#fff',
        position: 'absolute',
        zIndex: 1,
        bottom: 0,
        left: 0,
        right: 0,
        padding: 10,
        height: '65%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    heading: {
        alignSelf: 'center',
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
    },
    pickerContainer: {
        borderColor: '#DADADA',
        borderWidth: 1,
        borderRadius: 5,
        position: 'relative',
        marginTop: 20,
    },
    headePicker: {
        position: 'absolute',
        color: '#919191',
        backgroundColor: '#fff',
        top: -20,
        padding: 10,
        left: 20,
        fontFamily: 'Roboto-Regular',
    },
    picker: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
    },
    button: {
        backgroundColor: '#F15C20',
        borderRadius: 10,
        paddingHorizontal: 30,
        paddingVertical: 15,
        alignSelf: 'center',
        marginTop: 20
    },
    closeButton: {
        backgroundColor: '#F15C20',
        height: 35,
        width: 35,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 20
    }
})
