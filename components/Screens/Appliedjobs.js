import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, View, ScrollView, TouchableOpacity, Image } from 'react-native'
import Appliedjobscard from '../Reusable Components/Appliedjobscard'
import Navigationback from '../Reusable Components/Navigationback'
import Tabbar from '../Reusable Components/Tabbar'
import Loading from '../Screens/Loading'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import axios from 'axios'
import Drawerlayout from '../Reusable Components/Drawerlayout'
import { UserContext } from '../../AuthContext'


const Appliedjobs = (props) => {

    const [loading, setloading] = useState(false)
    const [appliedJobs, setappliedJobs] = useState([])
    const [showDrawer, setshowDrawer] = useState(false)
    const { signOut } = useContext(UserContext)

    const logOutUser = () => {
        signOut()
    }

    const getTheAppliedJob = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const response = await axios.get(constant.url + "appliedjobs/userappliedjobs/" + decoded.id, {
                headers: {
                    "x-access-token": token
                }
            })
            setappliedJobs(response.data.jobs)
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    useEffect(() => {
        getTheAppliedJob()
    }, [])

    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> :
                    <View style={styles.container}>
                        <Navigationback title="Applied Jobs" back={() => props.navigation.goBack()} />
                        <TouchableOpacity
                            onPress={() => setshowDrawer(true)}
                            style={styles.drawerbutton}>
                            <Image
                                style={{ height: 25, width: 25 }}
                                source={require('../../assets/icons/black_menu.png')} />
                        </TouchableOpacity>
                        {
                            showDrawer === false ? null : <Drawerlayout navigate={props.navigation.navigate}
                                logOutUser={logOutUser}
                                close={() => setshowDrawer(false)} />
                        }
                        <ScrollView contentContainerStyle={{ marginTop: 20 }} showsVerticalScrollIndicator={false}>
                            {
                                appliedJobs.map(data => {
                                    return <Appliedjobscard key={data._id}
                                        color="#F15C20"
                                        data={data}
                                        job={data.job_id}
                                        navigation={props.navigation}
                                        status="Shortlisted" />
                                })
                            }
                            <View style={{ marginBottom: 100 }} />
                        </ScrollView>
                        <Tabbar active="applied" navigate={props.navigation.navigate} />
                    </View>
            }
        </React.Fragment>
    )
}

export default Appliedjobs

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        paddingHorizontal: 10,
        backgroundColor: '#fff'
    },
    drawerbutton: {
        position: 'absolute',
        right: 10,
        top: 20,
        height: 40,
        width: 40,
        backgroundColor: '#F3F3F3',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
