import React, { useState, useEffect } from 'react'
import { Image, StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Edit1 from '../../assets/icons/edit1.svg'
import { Picker } from '@react-native-picker/picker';
import * as ImagePicker from 'react-native-image-picker';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage'
import Calender from '../../assets/icons/calendar.png'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Global from '../../Global'
import DateTimePicker from '@react-native-community/datetimepicker';
import Toast from 'react-native-simple-toast';

const Basicinformation = (props) => {

    const [image, setimage] = useState("https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png")
    const [name, setname] = useState(null)
    const [email, setemail] = useState(null)
    const [currentAddress, setcurrentAddress] = useState(null)
    const [premanentAdress, setpremanentAdress] = useState(null)
    const [dateOfBirth, setdateOfBirth] = useState(null)
    const [contact, setcontact] = useState(null)
    const [gender, setgender] = useState(null)
    const [loading, setloading] = useState(false)
    const [show, setshow] = useState(false)

    useEffect(() => {
        let date = formatDate(new Date())
        setname(Global.user.first_name)
        setemail(Global.user.email)
        if(Global.user.current_address !== undefined || Global.user.current_address !== null){
            setcurrentAddress(Global.user.current_address)
        }
        if(Global.user.permenant_address !== undefined || Global.user.permenant_address !== null){
            setpremanentAdress(Global.user.permenant_address)
        }
        if(Global.user.dob === null || Global.user.dob === undefined){
            setdateOfBirth(date)
        }else{
            setdateOfBirth(Global.user.dob)
        }
        setcontact(Global.user.mobile_no.toString())
        setgender(Global.user.gender)
        if(Global.user.profile_photo === null || Global.user.profile_photo === undefined){
            setimage("https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png")
        }else{
            setimage(Global.user.profile_photo)
        }
    }, [])

    const formatDate = (date) => {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setshow(Platform.OS === 'ios');
        let date = formatDate(currentDate)
        setdateOfBirth(date);
    };

    const selectTheImageFromGallaryOrCamera = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            ImagePicker.launchImageLibrary(
                {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxHeight: 1000,
                    maxWidth: 1000,
                }, (response) => {
                    if (response.didCancel !== true) {
                        const photo = {
                            uri: response.uri,
                            type: response.type,
                            name: response.fileName,
                        };

                        const config = {
                            headers: {
                                'content-type': 'multipart/form-data',
                                'x-access-token': token
                            }
                        }

                        let body = new FormData();
                        body.append('fileimage', photo);
                        body.append('userid', decoded.id);
                        setloading(true)
                        Axios.post(constant.url + "user/uploadUserimage/" + decoded.id, body, config).then(data => {
                            setimage(data.data.imageurl)
                            setloading(false)
                        })
                    } 
                })
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }


    const update = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }

            const body = {
                first_name: name,
                last_name: "",
                email: email,
                current_address: currentAddress,
                permenant_address: premanentAdress,
                dob: dateOfBirth,
                mobile_no: Number(contact),
                gender: gender,
                _id: decoded.id,
                //person_city_id: nativeCity,
            }

            await Axios.post(constant.url + "User/editUser", body, config)
            Toast.show("pull the page to refresh");
            props.navigation.goBack()
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    return (
        <React.Fragment>
            {
                loading === true ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size="large" color="#828282" />
                    </View> :
                    <ScrollView style={styles.container}>
                        <Navigationback title="Basic Information" back={() => props.navigation.goBack()} />
                        <TouchableOpacity style={styles.imageContainer} onPress={selectTheImageFromGallaryOrCamera}>
                            <Image
                                style={styles.image}
                                source={{ uri: image }}
                            />
                            <View style={styles.editProfile}>
                                <Edit1 width={10} height={10} />
                            </View>
                        </TouchableOpacity>

                        <View style={{ marginTop: 20, marginBottom: 20 }}>
                            <View style={styles.inputContainer}>
                                <TextInput value={name}
                                    placeholder="Enter your name"
                                    onChangeText={(text) => setname(text)} />
                                <Text style={styles.lable}>Name</Text>
                            </View>

                            <View style={styles.inputContainer}>
                                <TextInput value={email}
                                    placeholder="Enter your email"
                                    onChangeText={(text) => setemail(text)} />
                                <Text style={styles.lable}>Email</Text>
                            </View>

                            <View style={styles.inputContainer}>
                                <TextInput value={currentAddress}
                                    placeholder="Enter your current address"
                                    onChangeText={(text) => setcurrentAddress(text)} />
                                <Text style={styles.lable}>Current Address</Text>
                            </View>

                            <View style={styles.inputContainer}>
                                <TextInput value={premanentAdress}
                                    placeholder="Enter your permenent address"
                                    onChangeText={(text) => setpremanentAdress(text)} />
                                <Text style={styles.lable}>Permanent Address</Text>
                            </View>
                            {
                                /*<View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, alignItems: 'center' }}>
                                <Text>Same as Permanent address</Text>
                                <Switch
                                    trackColor={{ false: "#dcdcdc", true: "#81b0ff" }}
                                    thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={toggleSwitch}
                                    value={isEnabled}
                                />
                              </View>*/
                            }
                            <TouchableOpacity
                                onPress={() => setshow(true)}
                                style={[styles.inputContainer, { justifyContent: 'center' }]}>
                                <Text>{dateOfBirth}</Text>
                                <Image source={Calender} style={styles.calender} />
                                <Text style={styles.lable}>Date of birth</Text>
                            </TouchableOpacity>

                            <View style={styles.inputContainer}>
                                <TextInput value={contact}
                                    placeholder="Enter your mobile number"
                                    keyboardType="numeric"
                                    onChangeText={(text) => setcontact(text)} />
                                <Text style={styles.lable}>Contact</Text>
                            </View>

                            <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={gender}
                                    onValueChange={(itemValue) =>
                                        setgender(itemValue)
                                    }
                                >
                                    <Picker.Item label="Male" value="male" />
                                    <Picker.Item label="Female" value="female" />
                                    <Picker.Item label="Other" value="other" />
                                </Picker>
                                <Text style={styles.lable}>Gender</Text>
                            </View>

                            {/*

                            <View style={styles.inputContainer}>
                                <Picker
                                    value={nativeCity}
                                >
                                    <Picker.Item label="Pune" value="male" />
                                    <Picker.Item label="Nashik" value="female" />
                                    <Picker.Item label="Satra" value="other" />
                                </Picker>
                                <Text style={styles.lable}>Native City</Text>
                            </View>
                            
                <View style={styles.inputContainer}>
                    <Picker
                    value={userType}
                    >
                        <Picker.Item label="Male" value="male" />
                        <Picker.Item label="Female" value="female" />
                        <Picker.Item label="Other" value="other" />
                    </Picker>
                    <Text style={styles.lable}>User type</Text>
                </View>

                <Text style={{ marginTop: 10, fontSize: 16 }}>Social accounts</Text>

                <View style={styles.inputContainer}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <TextInput style={{ width: '80%' }} value={facebook} />
                    </View>
                    <Text style={styles.lable}>Facebook</Text>
                </View>

                <View style={styles.inputContainer}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <TextInput style={{ width: '80%' }} value={linkedIn} />
                        <Text style={{ color: '#F15C20' }}>Connect</Text>
                    </View>
                    <Text style={styles.lable}>Linkedin</Text>
                </View>*/}

                            <TouchableOpacity style={styles.button} onPress={update}>
                                <Text style={{ color: '#fff', fontSize: 16 }}>Update</Text>
                            </TouchableOpacity>

                        </View>
                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={new Date()}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                            />
                        )}
                    </ScrollView>
            }
        </React.Fragment>
    )
}

export default Basicinformation

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    imageContainer: {
        height: 100,
        width: 100,
        borderRadius: 100,
        position: 'relative',
        alignSelf: 'center',
        marginTop: 20
    },
    image: {
        height: 100,
        width: 100,
        borderRadius: 100,
        position: 'absolute'
    },
    editProfile: {
        height: 20,
        width: 20,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 5,
        right: 10,
        borderRadius: 20
    },
    inputContainer: {
        borderColor: '#DADADA',
        borderRadius: 5,
        borderWidth: 1,
        position: 'relative',
        paddingHorizontal: 10,
        marginTop: 25,
        height: 52
    },
    lable: {
        position: 'absolute',
        top: -14,
        color: '#919191',
        backgroundColor: '#fff',
        left: 30,
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#F15C20',
        borderRadius: 10,
        paddingHorizontal: 40,
        paddingVertical: 15,
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 20
    },
    calender: {
        width: 26,
        height: 26,
        position: 'absolute',
        bottom: 12,
        right: 10
    }
})
