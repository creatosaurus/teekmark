import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'

const Getstarted = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{`Find Your\nDream Job Here!`}</Text>
            <View style={styles.goContainer}>
                <Text style={styles.subTitle}>Let's go</Text>
            </View>
            <Image style={styles.banner} source={require('../../assets/icons/teekmark_job.png')} />
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.buttonLog}
                    onPress={() => props.navigation.navigate('login')}>
                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonLog}
                    onPress={() => props.navigation.navigate('signup')} >
                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Getstarted

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor: '#fff'
    },
    title: {
        fontSize: 30,
        fontFamily: 'Roboto-Bold',
        marginTop: "5%",
    },
    goContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: '2%'
    },
    subTitle: {
        fontFamily: 'Roboto-Medium',
        fontSize: 24,
        marginRight: 20
    },
    button: {
        backgroundColor: '#F15C20',
        height: 50,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    buttonLog: {
        backgroundColor: '#F15C20',
        height: 50,
        borderRadius: 10,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10
    },
    banner: {
        height: '65%',
        width: '100%',
        alignSelf: 'center',
        marginTop: '5%'
    },
    buttonContainer: {
        flexDirection: 'row',
        alignSelf: 'center'
    }
})
