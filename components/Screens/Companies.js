import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, TextInput, View, Text, ActivityIndicator, TouchableOpacity, Image } from 'react-native'
import Tabbar from '../Reusable Components/Tabbar'
import Navigationback from '../Reusable Components/Navigationback'
import { ScrollView } from 'react-native-gesture-handler'
import Jobcategorycard2 from '../Reusable Components/Jobcategorycard2'
import Companiescard from '../Reusable Components/Companiescard'
import Loading from './Loading'
import constant from '../../constant'
import Axios from 'axios'
import Arrow from '../../assets/icons/arrow.png'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Drawerlayout from '../Reusable Components/Drawerlayout'
import { UserContext } from '../../AuthContext'

const Companies = (props) => {

    const [popularCompanies, setpopularCompanies] = useState([])
    const [companiesForYour, setcompaniesForYour] = useState([])
    const [loadingPopularCompanies, setloadingPopularCompanies] = useState(false)
    const [loadingCompaniesForYou, setloadingCompaniesForYou] = useState(false)
    const [query, setquery] = useState(null)
    const [loading, setloading] = useState(false)
    const [showDrawer, setshowDrawer] = useState(false)
    const { signOut } = useContext(UserContext)

    const getPopularCompanies = async () => {
        try {
            setloadingPopularCompanies(true)
            const response = await Axios.get("http://teekmarkjobs.cloudjiffy.net/api/companyprofile/popularcompanies", {
                headers: {
                    "x-access-token": 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYTkyYzQ4MjVlZGQwNTg4MDZjZGM3NSIsImlhdCI6MTYxMDAzMjMwNiwiZXhwIjoxNjQxNTg5MjMyfQ.ouM-B1TzLUQiFQPMVZ5ftF5IdE-_caeeGbpvubYk2tQ'
                }
            })
            setpopularCompanies(response.data.company)
            setloadingPopularCompanies(false)
        } catch (error) {
            console.log(error)
            setloadingPopularCompanies(false)
        }
    }

    const getCompaniesForYou = async () => {
        try {
            setloadingCompaniesForYou(true)
            const response = await Axios.get("http://teekmarkjobs.cloudjiffy.net/api/companyprofile/companyforyou/5fdc6342d4bcfb3a388307d0", {
                headers: {
                    "x-access-token": 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmYTkyYzQ4MjVlZGQwNTg4MDZjZGM3NSIsImlhdCI6MTYxMDAzMjMwNiwiZXhwIjoxNjQxNTg5MjMyfQ.ouM-B1TzLUQiFQPMVZ5ftF5IdE-_caeeGbpvubYk2tQ'
                }
            })
            setcompaniesForYour(response.data)
            setloadingCompaniesForYou(false)
        } catch (error) {
            console.log(error)
            setloadingCompaniesForYou(false)
        }
    }

    useEffect(() => {
        if (popularCompanies.length === 0) {
            getPopularCompanies()
        }
        if (companiesForYour.length === 0) {
            getCompaniesForYou()
        }
    }, [])


    const filterTheCompanyJobs = async (company) => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                queryString: company
            }
            const response = await Axios.post(constant.url + "search/searchjobs/", body, config)
            props.navigation.navigate('search', {
                data: response.data,
                category: [],
                subCategory: [],
                designation: [],
                location: [],
            })
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    const searchTheCompany = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                queryString: query
            }
            const response = await Axios.post(constant.url + "search/searchjobs/", body, config)
            props.navigation.navigate('search', {
                data: response.data,
                category: [],
                subCategory: [],
                designation: [],
                location: [],
            })
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    const logOutUser = () => {
        signOut()
    }

    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> :
                    <View style={styles.container}>
                        {
                            showDrawer === false ? null : <Drawerlayout navigate={props.navigation.navigate}
                                logOutUser={logOutUser}
                                close={() => setshowDrawer(false)} />
                        }
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <Navigationback title="Companies" back={() => props.navigation.goBack()} />
                            <TouchableOpacity
                                onPress={() => setshowDrawer(true)}
                                style={styles.drawerbutton}>
                                <Image
                                    style={{ height: 25, width: 25 }}
                                    source={require('../../assets/icons/black_menu.png')} />
                            </TouchableOpacity>
                            <View style={styles.searchContainer}>
                                <TextInput placeholder="Search by company name"
                                    placeholderTextColor="#919191"
                                    onChangeText={(text) => setquery(text)}
                                    style={styles.search} />
                                <TouchableOpacity onPress={searchTheCompany} style={styles.arrowContainer}>
                                    <Image source={Arrow} style={styles.arrow} />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.popularJobs}>
                                <View style={styles.popularJobsTitleContainer}>
                                    <Text style={styles.jobTitle}>Popular Companies</Text>
                                </View>
                                {
                                    loadingPopularCompanies === true ? <ActivityIndicator size="large" color="#dcdcdc" /> :
                                        <ScrollView horizontal={true}
                                            showsHorizontalScrollIndicator={false}>
                                            {
                                                popularCompanies.map(data => {
                                                    return <Jobcategorycard2
                                                        key={data._id}
                                                        url={data.company_logo}
                                                        category={data.company_name}
                                                        filter={filterTheCompanyJobs} />
                                                })
                                            }
                                        </ScrollView>
                                }
                            </View>

                            <View style={{ ...styles.popularJobsTitleContainer, marginTop: 10 }}>
                                <Text style={styles.jobTitle}>Companies for you</Text>
                            </View>
                            {
                                loadingCompaniesForYou === true ? <ActivityIndicator size="large" color="#dcdcdc" /> :
                                    companiesForYour.map(data => {
                                        return <Companiescard key={data._id}
                                            onPress={() => props.navigation.navigate("availablejobs", {
                                                data: data
                                            })}
                                            companyName={data.company_name}
                                            location={data.address}
                                            updatedAt={data.updatedAt}
                                            opening={data.openingcount}
                                            url={data.company_logo}
                                        />
                                    })
                            }
                            <View style={{ marginBottom: 80 }} />
                        </ScrollView>
                        <Tabbar active="companies" navigate={props.navigation.navigate} />
                    </View>
            }
        </React.Fragment>
    )
}

export default Companies

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        position: 'relative',
        paddingHorizontal: 10
    },
    searchContainer: {
        marginTop: 20,
        position: 'relative',
        height: 50,
    },
    search: {
        backgroundColor: '#F4F6F7',
        borderRadius: 10,
        paddingHorizontal: 10,
    },
    filter: {
        backgroundColor: "#F15C20",
        height: 50,
        width: 50,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    popularJobs: {
        marginTop: 20,
    },
    popularJobsTitleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10
    },
    jobTitle: {
        fontSize: 20,
        fontFamily: 'Roboto-Medium',
    },
    jobSubTitle: {
        color: '#919191',
        fontFamily: 'Roboto-Regular',
    },
    arrowContainer: {
        position: 'absolute',
        right: 20,
        top: 10,
        width: 60,
        alignItems: 'flex-end'
    },
    arrow: {
        width: 30,
        height: 30,
    },
    drawerbutton: {
        position: 'absolute',
        right: 0,
        top: 20,
        height: 40,
        width: 40,
        backgroundColor: '#F3F3F3',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
