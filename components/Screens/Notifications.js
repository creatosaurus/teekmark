import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Notificationcard from '../Reusable Components/Notificationcard'
import Loading from './Loading'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode"

const Notifications = (props) => {

    const [notifications, setnotifications] = useState([])
    const [loading, setloading] = useState(false)

    useEffect(() => {
        getTheNotification()
    }, [])

    const getTheNotification = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const response = await Axios.get(constant.url + "notification/getNotification/" + decoded.id, {
                headers: {
                    "x-access-token": token
                }
            })
            setnotifications(response.data)
            setloading(false)
        } catch (error) {
            console.log(error)
            setloading(false)
        }
    }

    const deleteNotification = async (id) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const filterArray = notifications.filter((data) => data._id !== id)
            setnotifications(filterArray)
            await Axios.delete(constant.url + "notification/removeNotification/" + id, {
                headers: {
                    "x-access-token": token
                }
            })
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> :
                    <View style={styles.container}>
                        <Navigationback title="Notifications" back={() => props.navigation.goBack()} />
                        <ScrollView>
                            {
                                notifications.map(data => {
                                    return <Notificationcard
                                        onPress={() => deleteNotification(data._id)}
                                        title={data.title}
                                        description={data.description}
                                        id={data._id}
                                        key={data._id} />
                                })
                            }
                        </ScrollView>
                    </View>
            }
        </React.Fragment>
    )
}

export default Notifications

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 10
    },
    buttonClear: {
        color: '#919191',
        alignSelf: 'flex-end'
    }
})
