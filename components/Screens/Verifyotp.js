import React, { useState, useContext } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import InputBox from '../Reusable Components/InputBox'
import Logo from '../../assets/icons/Logo.png'
import Axios from 'axios'
import constant from '../../constant'
import { UserContext } from '../../AuthContext'
import AsyncStorage from '@react-native-async-storage/async-storage'

const Verifyotp = (props) => {

    const [otp, setOtp] = useState(null)
    const { logIn } = useContext(UserContext)

    const verifyUserOTP = async () => {
        try {
            if (otp === null) return alert("plz enter the OTP")
            const response = await Axios.post(constant.url + "user/verifyotp", {
                email: props.route.params.email,
                register_otp: Number(otp)
            })
            if (response.status === 200) {
                await AsyncStorage.setItem("token", response.data.token)
                logIn()
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Image source={Logo} style={styles.logo} />
                <Text style={styles.headerTitle}>Teekmark</Text>
            </View>
            <View style={styles.inputContainer}>
                <Text style={styles.inputContainerTitle}>OTP</Text>
                <Text style={styles.inputContainerSubTitle}>You may have received your OTP on your register mobile number plz enter OTP to conform</Text>
                <InputBox
                    placeholder="Enter your OTP"
                    title="OTP"
                    keyboardType="numeric"
                    onChangeText={text => setOtp(text)} />
            </View>
            <View style={styles.bottomContainer}>
                <TouchableOpacity style={styles.button} onPress={verifyUserOTP}>
                    <Text style={{ color: '#fff' }}>Conform OTP</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.footerTitle}>{`Copyright @ 2020. TeekmarkAll\nrights reserved`}</Text>
        </View>
    )
}

export default Verifyotp

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        position: 'relative'
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 10
    },
    headerTitle: {
        fontSize: 20,
        fontFamily: 'Poppins-Medium'
    },
    inputContainer: {
        marginTop: '4%'
    },
    inputContainerTitle: {
        fontSize: 26,
        fontFamily: 'Roboto-Bold'
    },
    inputContainerSubTitle: {
        fontSize: 18,
        fontFamily: 'Roboto-Light',
        marginVertical: 10,
    },
    bottomContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    forgot: {
        alignSelf: 'flex-end',
        fontFamily: 'Roboto-Regular'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: '40%',
        backgroundColor: '#F15C20',
        borderRadius: 10
    },
    linkContainer: {
        flexDirection: 'row',
        marginTop: 20
    },
    info: {
        fontSize: 16,
        fontFamily: "Roboto-Medium",
        color: '#000'
    },
    login: {
        color: '#F15C20',
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
    },
    footerTitle: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center',
        position: 'absolute',
        bottom: 20,
        alignSelf: 'center'
    },
    logo: {
        width: 60,
        height: 45
    }
})