import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import secondsToDhms from '../Functions/Comparetime'

const Jobsforyoucard = (props) => {
    let time
    time = secondsToDhms(props.data.updatedAt)
    return (
        <TouchableOpacity style={styles.container}
            onPress={() => props.navigation.navigate("jobdescription", {
                data: props.data
            })}
        >
            <View style={styles.subContainer}>
                <Image source={{ uri: props.data.company_id.company_logo }}
                    style={{ width: 30, height: 30 }} />
                <View style={{ marginLeft: 15 }}>
                    <Text style={styles.Title}>{props.data.title}</Text>
                    <Text style={styles.Subtitle}>{props.data.company_id.company_name}</Text>
                </View>
            </View>
            <View style={{flexDirection:'column', alignItems:'center'}}>
                <Text style={styles.salary}>{props.data.ctc}</Text>
                <Text style={{ fontSize: 8, marginTop:5 }}>{time}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default Jobsforyoucard

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        padding: 10,
        borderColor: "#00000038",
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 5,
        marginTop: 10,
        zIndex: 0,
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    Title: {
        fontFamily: 'Roboto-Medium',
    },
    Subtitle: {
        color: '#8B8B8B',
        fontSize: 13,
        fontFamily: 'Roboto-Regular',
    },
    salary: {
        backgroundColor: '#F4F6F7',
        fontFamily: 'Roboto-Regular',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5
    }
})
