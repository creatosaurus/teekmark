import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const Appliedjobscard = (props) => {
    return (
        <TouchableOpacity
            onPress={() => props.navigation.navigate("jobdescription", {
                data: props.job,
                applied: true
            })}
            style={styles.container}>
            <View style={styles.subContainer}>
                <View style={{ marginLeft: 15 }}>
                    <Text style={styles.title}>{props.data.company_id.company_name}</Text>
                    <Text style={styles.subTitle}>{props.data.job_id.title}</Text>
                    <Text style={styles.location}>{props.data.job_id.city_id.city}</Text>
                </View>
            </View>
            <View style={styles.status}>
                <Text style={{ color: props.color }}>{props.data.job_application_status}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default Appliedjobscard

const styles = StyleSheet.create({
    container: {
        borderColor: '#CFCFCF',
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        color: '#8B8B8B',
        fontSize: 13,
        fontFamily: 'Roboto-Regular',
    },
    subTitle: {
        fontFamily: 'Roboto-Medium',
    },
    location: {
        color: '#747474',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },
    salary: {
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },
    status: {
        borderColor: '#ECECEC',
        borderWidth: 1,
        borderRadius: 10,
        paddingHorizontal: 20,
        paddingVertical: 10
    }
})
