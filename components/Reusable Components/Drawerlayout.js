import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Linking, Image } from 'react-native'
import Logout from '../../assets/icons/logout.svg'
import Padlock from '../../assets/icons/padlock.svg'
import Terms from '../../assets/icons/text-document.svg'
import Share from '../../assets/icons/share.svg'
import Star from '../../assets/icons/star.svg'
import Settings from '../../assets/icons/settings1.svg'
import Global from '../../Global'

const Drawerlayout = (props) => {
    return (
        <View style={styles.container}>
            <View style={{ padding: 20, borderBottomColor: '#747474', borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Text style={{ color: '#747474', fontSize: 19, fontFamily: 'Roboto-Medium' }}>{Global.user === null ? null : Global.user.first_name}</Text>
                <TouchableOpacity style={{ padding: 10 }} onPress={props.close}>
                    <Text style={{ fontSize: 20, color: '#747474' }}>X</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.subContainer} onPress={() => props.navigate('settings')}>
                <Settings width={15} height={15} />
                <Text style={styles.title}>Settings</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => Linking.openURL('http://google.com')}
                style={styles.subContainer}>
                <Star width={15} height={15} />
                <Text style={styles.title}>Rate App</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => Linking.openURL('http://google.com')}
                style={styles.subContainer}>
                <Share width={15} height={15} />
                <Text style={styles.title}>Share App</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => Linking.openURL('http://google.com')}
                style={styles.subContainer}>
                <Terms width={15} height={15} />
                <Text style={styles.title}>Terms and Service</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => Linking.openURL('http://google.com')}
                style={styles.subContainer}>
                <Padlock width={15} height={15} />
                <Text style={styles.title}>Privacy Policy</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.subContainer} onPress={() => props.logOutUser()}>
                <Logout width={15} height={15} />
                <Text style={styles.title}>Logout</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Drawerlayout

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        width: 220,
        position: 'absolute',
        height: '100%',
        right: 0,
        zIndex:100
    },
    subContainer: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        fontSize: 16,
        marginLeft: 10
    }
})
