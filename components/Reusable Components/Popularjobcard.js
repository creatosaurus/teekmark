import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import secondsToDhms from '../Functions/Comparetime'

const Popularjobcard = (props) => {
    let time
    time = secondsToDhms(props.data.updatedAt)
    return (
        <TouchableOpacity style={styles.container} onPress={() => props.navigation.navigate("jobdescription", {
            data: props.data
        })}>
            <View style={styles.subContainer}>
                <View>
                    <Text style={styles.companyName}>{props.companyName}</Text>
                    <Text style={styles.jobRole}>{props.jobRole}</Text>
                </View>
                <Image style={{ marginLeft: 50, width: 40, height: 40 }} source={{ uri: props.url }} />
            </View>
            <View style={styles.subContainer}>
                <View>
                    <Text style={styles.location}>{props.location}</Text>
                    <Text style={styles.salary}>{props.salary}</Text>
                </View>
                <Text style={{ fontSize: 8 }}>{time}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default Popularjobcard

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#EDDCD285',
        paddingHorizontal: 10,
        paddingVertical: 20,
        borderRadius: 10,
        marginRight: 20
    },
    subContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    companyName: {
        fontFamily: 'Roboto-Regular',
    },
    jobRole: {
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
        marginTop: 5
    },
    location: {
        fontFamily: 'Roboto-Light',
        marginTop: 10
    },
    salary: {
        backgroundColor: '#fff',
        alignSelf: 'flex-start',
        paddingHorizontal: 10,
        marginTop: 10,
        borderRadius: 5,
        fontFamily: 'Roboto-Regular',
    }
})
