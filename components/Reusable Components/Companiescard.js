import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import secondsToDhms from '../Functions/Comparetime'

const Companiescard = (props) => {

    let time
    time = secondsToDhms(props.updatedAt)

    return (
        <TouchableOpacity style={styles.container1} {...props}>
            <View style={styles.container}>
                <View style={styles.subContainer}>
                    <Image style={{ width: 35, height: 35 }} source={{ uri: props.url }} />
                    <View style={{ marginLeft: 15 }}>
                        <Text style={styles.title}>{props.companyName}</Text>
                        <Text style={styles.location}>{props.location}</Text>
                        <Text style={styles.opening}>{props.opening} Openings</Text>
                    </View>
                </View>
            </View>

            <View style={styles.tagContainer}>
                {
                    props.jobtype === undefined ? null : <View style={styles.tagWrapper}>
                        <Text style={{ fontSize: 12 }}>{props.jobtype}</Text>
                    </View>
                }
                {
                    props.duration === undefined ? null : <View style={styles.tagWrapper}>
                        <Text style={{ fontSize: 12 }}>{props.duration}</Text>
                    </View>
                }
            </View>
            <Text style={{ fontSize: 8, position: 'absolute', bottom: 20, right: 10 }}>{time}</Text>
        </TouchableOpacity>
    )
}

export default Companiescard

const styles = StyleSheet.create({
    container1: {
        borderColor: '#CFCFCF',
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        marginTop: 10,
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        color: '#8B8B8B',
        fontSize: 13,
        fontFamily: 'Roboto-Regular',
    },
    location: {
        color: '#747474',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },
    opening: {
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        color: '#000'
    },
    tagContainer: {
        flexDirection: 'row',
        marginTop: 10,
    },
    tagWrapper: {
        backgroundColor: '#F4F6F7',
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 5,
        marginRight: 10
    }
})