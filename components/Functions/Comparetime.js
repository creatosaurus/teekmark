const secondsToDhms = (date) => {
    let date1 = new Date(date)
    let date2 = new Date()
    let seconds = (date2.getTime() - date1.getTime()) / 1000;
    seconds = Number(seconds);
    var d = Math.floor(seconds / (3600 * 24));
    var h = Math.floor(seconds % (3600 * 24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 60);

    if (s < 60 && h === 0 && m === 0 && d === 0) {
        return s + " sec ago"
    } else if (m < 60 && h === 0 && d === 0) {
        return m + " min ago"
    } else if (h < 24 && d === 0) {
        return h + " hours ago"
    } else {
        return d + " days ago"
    }
}

export default secondsToDhms